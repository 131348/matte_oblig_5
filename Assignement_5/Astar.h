#pragma once
#include <unordered_map>
#include "SFML\Graphics.hpp"
#include "Tilemap.h"


struct Node {
	sf::Vector2u pos;
	int gCost;
	int hCost;
	Node* parent;

	Tile* tile;

	Node(Tile* tile, const sf::Vector2u& pos) : tile(tile), pos(pos)  {
		gCost = 0;
		hCost = 0;
	}

	int FCost() const {
		return gCost + hCost;
	}
};

class Astar {
private:
	std::unordered_map<unsigned int, Node> nodes;

	std::vector<Node*> openSet;
	std::vector<Node*> closedSet;

	std::vector<Node*> tracePath;
	
	sf::Vector2u mapDimensions;

	std::vector<Node*> GetNeighbours(const sf::Vector2u& pos);
	int GetDistance(Node* a, Node* b);

public:
	Astar(std::vector<Tile>& tiles, const sf::Vector2u& dimensions);

	void FindPath(const sf::Vector2u& from, const sf::Vector2u to);
	void UpdateColors();
};
