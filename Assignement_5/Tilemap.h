#pragma once
#include "SFML\Graphics.hpp"
#include "Tile.h"
#include <vector>
#include <iostream>

class Tilemap {
private:
	const sf::Vector2u dimension = { 32, 32 };

	std::vector<Tile> tilemap;
	sf::Vector2f tileSize;
	std::pair<bool, Tile&> GetTileFromScreenCords(const sf::Vector2i& screenPos);

public:
	Tilemap(const sf::Vector2u& screenSize);

	void HandleEvents(sf::Event& evt);
	void Draw(sf::RenderWindow& window);

	Tile* GetTile(const sf::Vector2u& pos);
	std::vector<Tile>& GetTileList();
	const sf::Vector2u& GetDimensions();

	void Update();
};