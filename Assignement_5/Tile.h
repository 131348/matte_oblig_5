#pragma once

struct Tile {
	bool traversable;
	int gCost;
	int hCost;
	Tile* parent;

	sf::Color drawColor;

	Tile() {
		traversable = true;
		drawColor = sf::Color::Red;
		gCost = 0;
		hCost = 0;
	}

	void ToggleTraversable() {
		traversable = !traversable;
		drawColor = traversable ? sf::Color::Red : sf::Color(45, 45, 45, 0);
	}

	int FCost() const {
		return gCost + hCost;
	}
};
