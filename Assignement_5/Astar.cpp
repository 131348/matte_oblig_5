#include "Astar.h"
#include <algorithm>
#include <math.h>

Astar::Astar(std::vector<Tile>& tiles, const sf::Vector2u& dimensions) : mapDimensions(dimensions) {
	for (int i = 0; i < tiles.size(); i++) {
		this->nodes.emplace(i, Node(&tiles[i], { i % dimensions.x, i / dimensions.y }));
	}
}

std::vector<Node*> Astar::GetNeighbours(const sf::Vector2u& pos) {
	std::vector<Node*> neighbours;
	for (int y = -1; y <= 1; y++) {
		for (int x = -1; x <= 1; x++) {
			if (y == 0 && x == 0 || (y != 0 && x != 0))
				continue;
			sf::Vector2i checkPos = sf::Vector2i(pos) + sf::Vector2i(x, y);
			if (checkPos.y >= 0 && checkPos.y < this->mapDimensions.y && checkPos.x >= 0 && checkPos.x < this->mapDimensions.x) {
				neighbours.push_back(&(this->nodes.find(checkPos.y * this->mapDimensions.x + checkPos.x)->second));
			}
		}
	}
	if (neighbours.empty())
		std::cout << "wtf";
	return neighbours;
}

int Astar::GetDistance(Node* a, Node* b) {
	sf::Vector2i dist = {abs((int)(a->pos.x - b->pos.x)), abs((int)(a->pos.y - b->pos.y))};
	return (dist.x + dist.y) * 10;
	// this is completly unnesecary when their are no diagonals 
}

void Astar::FindPath(const sf::Vector2u& from, const sf::Vector2u to) {
	this->tracePath.clear();
	this->openSet.clear();
	this->closedSet.clear();

	auto startNode = this->nodes.find(from.y * this->mapDimensions.x + from.x);
	auto goalNode = this->nodes.find(to.y * this->mapDimensions.x + to.x);

	if (startNode == this->nodes.end() || goalNode == this->nodes.end())
		return;

	this->openSet.emplace_back(&(startNode->second));

	while (!this->openSet.empty()) {
		Node* currentNode = this->openSet.front();

		for (auto it = this->openSet.begin() + 1; it != this->openSet.end(); it++) {
			if ((*it)->FCost() <= currentNode->FCost() || (*it)->FCost() == currentNode->FCost() && (*it)->hCost < currentNode->hCost) {
				currentNode = (*it);
			}
		}

		this->openSet.erase(std::find(this->openSet.begin(), this->openSet.end(), currentNode));
		this->closedSet.emplace_back(currentNode);

		if (currentNode->pos == goalNode->second.pos) {
			Node* traceNode = &(goalNode->second);
			while (traceNode->pos != startNode->second.pos) {
				this->tracePath.emplace_back(traceNode);
				traceNode = traceNode->parent;
			}

			this->tracePath.emplace_back(&(startNode->second));
			std::reverse(this->tracePath.begin(), this->tracePath.end());
			return;
		}
		
		for (auto it : GetNeighbours(currentNode->pos)) {
			if (!it->tile->traversable || std::find(this->closedSet.begin(), this->closedSet.end(), it) != this->closedSet.end())
				continue;

			int newCostToNeighbour = currentNode->gCost + GetDistance(currentNode, it);
			if (newCostToNeighbour < it->gCost || std::find(this->openSet.begin(), this->openSet.end(), it) == this->openSet.end()) {
				it->gCost = newCostToNeighbour;
				it->hCost = GetDistance(it, &(goalNode->second));
				it->parent = currentNode;

				if (std::find(this->openSet.begin(), this->openSet.end(), it) == this->openSet.end()) {
					this->openSet.emplace_back(it);
				}
			}
		}
	}
}

void Astar::UpdateColors() {
	for (auto it : this->closedSet) {
		it->tile->drawColor = sf::Color::Blue;
	}

	for (auto it : this->openSet) {
		it->tile->drawColor = sf::Color::Cyan;
	}

	for (auto it : this->tracePath) {
		it->tile->drawColor = sf::Color::Yellow;
	}
}