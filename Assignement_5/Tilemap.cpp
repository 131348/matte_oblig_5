#include "Tilemap.h"
#include <algorithm>


Tilemap::Tilemap(const sf::Vector2u& screenSize) :
	tileSize({(float)screenSize.x / this->dimension.x, (float)screenSize.y / this->dimension.y}) {
	for (int i = 0; i < this->dimension.x * this->dimension.y; i++) {
		this->tilemap.emplace_back(Tile());
	}
}

std::pair<bool, Tile&> Tilemap::GetTileFromScreenCords(const sf::Vector2i& screenPos) {
	sf::Vector2i tilePos{ (int)(screenPos.x / this->tileSize.x), (int)(screenPos.y / this->tileSize.y) };
	if (tilePos.x < 0 || tilePos.x > this->dimension.x - 1
		|| tilePos.y < 0 || tilePos.y > this->dimension.y - 1) {
		return { false, Tile() };
	}
	return { true, this->tilemap[tilePos.y * this->dimension.x + tilePos.x] };
}

void Tilemap::HandleEvents(sf::Event& evt) {
	if (evt.type == sf::Event::MouseButtonReleased) {
		if (evt.mouseButton.button == sf::Mouse::Left) {
			std::pair<bool, Tile&> clickedTile = GetTileFromScreenCords({ evt.mouseButton.x, evt.mouseButton.y });
			if (clickedTile.first) {
				clickedTile.second.ToggleTraversable();
			}
		}
	}
}

void Tilemap::Update() {
	for (auto& it : this->tilemap) {
		if (it.traversable)
			it.drawColor = sf::Color::Red;
	}
}

void Tilemap::Draw(sf::RenderWindow& window) {
	std::pair<bool, Tile&> hoveredTile = GetTileFromScreenCords(sf::Mouse::getPosition(window));
	if (hoveredTile.first) {
		hoveredTile.second.drawColor.a = 170.f;
	}

	sf::Vector2f borderThickness = { 2.f, 2.f };
	sf::RectangleShape drawShape(sf::Vector2f(this->tileSize) - borderThickness);
	for (int i = 0; i < this->tilemap.size(); i++) {
		drawShape.setPosition(sf::Vector2f({ (i % this->dimension.x) * this->tileSize.x, i / this->dimension.x * this->tileSize.y}));
		drawShape.setFillColor(this->tilemap[i].drawColor);
		window.draw(drawShape);
	}

	if (hoveredTile.first) {
		hoveredTile.second.drawColor.a = 255.f;
	}
}

Tile* Tilemap::GetTile(const sf::Vector2u& pos) {
	return &(this->tilemap[pos.y * this->dimension.x + pos.x]);
}

std::vector<Tile>& Tilemap::GetTileList() {
	return this->tilemap;
}

const sf::Vector2u& Tilemap::GetDimensions() {
	return this->dimension;
}
