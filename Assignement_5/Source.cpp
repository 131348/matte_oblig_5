#include "SFML\Graphics.hpp"
#include "Tilemap.h"
#include <unordered_map>
#include "Astar.h"

int main() {
	const sf::Vector2u RESOLUTION = sf::Vector2u(640, 640);
	sf::RenderWindow window(sf::VideoMode(RESOLUTION.x, RESOLUTION.y), "Astar");


	sf::Clock clock;
	sf::Time elapsed;
	
	Tilemap tilemap(RESOLUTION);

	Astar astar(tilemap.GetTileList(), tilemap.GetDimensions());

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			
			tilemap.HandleEvents(event);
		}

		sf::Time timestep = sf::seconds(1.0f / 60);
		elapsed += clock.restart();

		if (elapsed >= timestep) {
			tilemap.Update();
			astar.FindPath({ 0,0 }, { 31,31 });
			astar.UpdateColors();

			window.clear();
			tilemap.Draw(window);
			window.display();

			elapsed -= timestep;
		}
		clock.restart();


	}

	return 0;
}